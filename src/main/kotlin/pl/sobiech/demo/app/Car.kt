package pl.sobiech.demo.app

class Car(private val house: House) {

    fun driveToHouse() {
        println("Driving to $house")
    }
}

package pl.sobiech.demo.app

fun main() {
    val house = House("Edenshire")
    Car(house).driveToHouse()
}

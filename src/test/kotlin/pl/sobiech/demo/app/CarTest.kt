package pl.sobiech.demo.app

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

internal class CarTest {

    private val house = mockk<House>()

    private val car = Car(house)

    @Test
    fun `Should load app`() {
        every { house.toString() } returns "Mock House"
        car.driveToHouse()
    }

    @ParameterizedTest
    @ArgumentsSource(SampleArgumentsProvider::class)
    fun `Some parameterized test`(name: String, expectedLength: Int) {

        with(SoftAssertions()) {
            assertThat(name).isEqualTo("Volvo")
            assertThat(name.length).isEqualTo(expectedLength)
            assertAll()
        }
    }
}

internal object SampleArgumentsProvider : ArgumentsProvider {

    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
        Arguments.of("Volvo", 5),
    )
}

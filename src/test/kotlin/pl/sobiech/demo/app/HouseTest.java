package pl.sobiech.demo.app;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class HouseTest {

    private final House house = new House("Crystal Avenue");

    @Test
    void shouldReturnHouseName() {
        assertThat(house.getName()).isEqualTo("Crystal Avenue");
    }
}

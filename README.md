# Kotlin Demo App

Kotlin training console app with basic utilities (unit testing) and code samples. You can use Java interchangeably with Kotlin in this app, so you can write and test comparisons between the two languages.

### Prerequisites 
* Apache Maven (newest version)
* JDK 11 or newer

### Build
To prepare an executable JAR run: `mvn package`

### Test
To run tests execute: `mvn clean test`

### Run
To run the app execute: `java -jar target/DemoApp-0.1.jar`

### Dependencies:

* Kotlin **1.6.10**
* JUnit 5 Jupiter **5.8.2**
* AssertJ Core **3.22.0**
* Mockk **1.12.3**

### Useful Links

* https://kotlinlang.org/docs/home.html
* https://mockk.io